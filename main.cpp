#include <iostream>
using namespace std;

struct Node
{
    int value;
    Node * left;
    Node * right;
    Node(): left{nullptr}, right{nullptr}, value{0} {};
};

int nextNode( Node * tree, int value )
{
    if( !tree )
        return value;

    int curr = tree->value;

    if( tree->right && value >= tree->value )
        curr = nextNode( tree->right, value );
    if( tree->left && value < tree->value )
    {
        int next = nextNode( tree->left, value );
        curr = next > value ? next : curr;
    }
    return curr > value ? curr : value;
}

int prevNode( Node * tree, int value )
{
    if( !tree )
        return value;

    int curr = tree->value;

    if( tree->left && value < tree->value )
        curr = prevNode( tree->left, value );
    if( tree->right && value >= tree->value )
    {
        int next = prevNode( tree->right, value );
        curr = next < value ? next : curr;
    }
    return curr < value ? curr : value;
}

bool existsNode( Node *& tree, int value )
{
    if( !tree )
        return false;
    if( tree->value == value)
        return true;
    bool res = false;
    if( value < tree->value )
        res = existsNode( tree->left, value );
    else if( value > tree->value)
        res = res || existsNode(tree->right, value);
}

void insertNode( Node *& tree, int value )
{
    if( !tree )
    {
        tree = new Node;
        tree->value = value;
    }
    else
    {
        if( value > tree->value )
            insertNode( tree->right, value );
        else
            insertNode( tree->left, value );
    }
}


void deleteSpecificNode( Node *& node )
{

    if( node->left == nullptr )
    {
        Node * tmp = node;
        node = node->right;
        delete tmp;
    }
    else if( node->right == nullptr )
    {
        Node * tmp = node;
        node = node->left;
        delete tmp;
    }
    else
    {
        Node * minParent = node;
        Node * min = node->right;
        while( min->left != 0 )
        {
            minParent = min;
            min = min->left;
        }
        Node * tmp1 = node;
        if( node->right == min )
        {
            node = min;
            node->left = tmp1->left;
            delete tmp1;
        } else
        {
            minParent->left = min->right;
            min->right = node->right;
            node = min;
            node->left = tmp1->left;
            delete  tmp1;
        }
    }
}

void deleteNode( Node *& tree, int value )
{
    if( !tree )
        return;
    if( tree->value == value )
    {
       deleteSpecificNode( tree );
       return;
    }
    deleteNode( tree->value > value ? tree->left : tree->right, value);
}
//=======
int countNode(Node*tree)
{
    if(!tree)
        return 0;
    int left=countNode(tree->left);
    int right=countNode(tree->right);
    return left+right+1;

}

void show( Node * tree, int leftChild=2 )
{
    int rightChild;
    if(!tree->left)
        leftChild=0;
    if (!tree->right)
        rightChild=0;
    else
        rightChild=leftChild+countNode(tree->right);

    cout << tree->value<<" "<< leftChild<<" "<<leftChild+countNode(tree->left)<<endl;


    if( tree->left )
        show( tree->left, leftChild+1 );
    if( tree->right )
        show( tree->right, rightChild+1 );
}
///=======//
void destroyTree( Node * node )
{
    if( !node )
        return;
    if( node->left )
        destroyTree( node->left );
    if( node->right )
        destroyTree( node->right );

    delete node;
}


int main() {
    Node * tree = nullptr;
    insertNode( tree, 50);
    insertNode( tree, 25);
    insertNode( tree, 100);
    insertNode( tree, 200);
    insertNode( tree, 11);
    insertNode( tree, 40);
    insertNode( tree, 30);
    insertNode( tree, 45);
    insertNode( tree, 35);
    insertNode( tree, 36);
    deleteNode( tree, 50);
//    deleteNode( tree, 40);
    show(tree);
    cout<<endl;
    cout << prevNode( tree, 49);
    return 0;
}
